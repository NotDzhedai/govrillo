package bluesky

import (
	"context"
	"fmt"

	"github.com/bluesky-social/indigo/api/bsky"
)

const limit = 100

type Reply struct {
	DisplayName string
}

type BskyPost struct {
	CreatedAt         string
	AuthorHandle      string
	AuthorDisplayName string
	Text              string
	LikeCount         int64
	ReplayCount       int64
	RepostCount       int64
	Images            []string
	Replay            *Reply
	Uri               string
	Cid               string
	IsRoot            bool
}

func (a *Client) GetAuthorFeed() ([]BskyPost, error) {
	var feed []*bsky.FeedDefs_FeedViewPost
	var cursor string

	for {
		resp, err := bsky.FeedGetAuthorFeed(
			context.Background(),
			a.xrpclient,
			a.xrpclient.Auth.Handle,
			cursor,
			"posts_no_replies",
			limit,
		)
		if err != nil {
			return nil, err
		}

		feed = append(feed, resp.Feed...)
		if resp.Cursor == nil {
			break
		}
		cursor = *resp.Cursor
	}

	var result []BskyPost
	for _, f := range feed {
		p := f.Post
		rec := p.Record.Val.(*bsky.FeedPost)

		if rec.Entities != nil {
			fmt.Println(rec.Entities)
		}

		parsedTime, err := parseDate(rec.CreatedAt)
		if err != nil {
			return nil, err
		}

		bskyPost := BskyPost{
			CreatedAt:         parsedTime,
			AuthorHandle:      p.Author.Handle,
			AuthorDisplayName: *p.Author.DisplayName,
			Text:              rec.Text,
			LikeCount:         *p.LikeCount,
			ReplayCount:       *p.ReplyCount,
			RepostCount:       *p.RepostCount,
			Images:            make([]string, 0),
			Replay:            nil,
			Uri:               p.Uri,
			Cid:               p.Cid,
			IsRoot:            true,
		}

		if p.Embed != nil {
			if p.Embed.EmbedImages_View != nil {
				for _, image := range p.Embed.EmbedImages_View.Images {
					bskyPost.Images = append(bskyPost.Images, image.Fullsize)
				}
			}
			if f.Post.Embed.EmbedRecordWithMedia_View != nil {
				for _, image := range f.Post.Embed.EmbedRecordWithMedia_View.Media.EmbedImages_View.Images {
					bskyPost.Images = append(bskyPost.Images, image.Fullsize)
				}
			}
		}

		if f.Reply != nil && f.Reply.Parent != nil {
			bskyPost.Replay = &Reply{DisplayName: *f.Reply.Parent.FeedDefs_PostView.Author.DisplayName}
		}

		result = append(result, bskyPost)
	}

	return result, nil
}

func (c *Client) GetPostThread(postUri string) ([]BskyPost, error) {
	resp, err := bsky.FeedGetPostThread(
		context.Background(),
		c.xrpclient,
		100,
		100,
		postUri,
	)
	if err != nil {
		return nil, err
	}

	result := make([]BskyPost, 0)
	for _, item := range resp.Thread.FeedDefs_ThreadViewPost.Replies {
		post := item.FeedDefs_ThreadViewPost.Post
		rec := post.Record.Val.(*bsky.FeedPost)

		parsedTime, err := parseDate(rec.CreatedAt)
		if err != nil {
			return nil, err
		}

		result = append(result, BskyPost{
			CreatedAt:         parsedTime,
			AuthorHandle:      post.Author.Handle,
			AuthorDisplayName: *post.Author.DisplayName,
			Text:              rec.Text,
			LikeCount:         *post.LikeCount,
			ReplayCount:       *post.ReplyCount,
			RepostCount:       *post.RepostCount,
			Images:            make([]string, 0),
			Replay:            nil,
			Uri:               post.Uri,
			Cid:               post.Cid,
			IsRoot:            false,
		})
	}

	return result, nil
}
