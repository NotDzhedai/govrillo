package bluesky

import (
	"context"
	"encoding/json"
	"os"
	"time"

	"github.com/bluesky-social/indigo/api/atproto"
	"github.com/bluesky-social/indigo/util/cliutil"
	"github.com/bluesky-social/indigo/xrpc"
)

const authFile = ".auth.json"

func createXrpcClient(ctx context.Context, handle, password string) (*xrpc.Client, error) {
	xrpcc := &xrpc.Client{
		Host: Host,
	}

	if client, err := tryCreateXrpcClient(ctx); err == nil {
		return client, nil
	}

	auth, err := atproto.ServerCreateSession(ctx, xrpcc, &atproto.ServerCreateSession_Input{
		Identifier: handle,
		Password:   password,
	})
	if err != nil {
		return nil, err
	}

	xrpcc.Auth = &xrpc.AuthInfo{
		AccessJwt:  auth.AccessJwt,
		RefreshJwt: auth.RefreshJwt,
		Did:        auth.Did,
		Handle:     auth.Handle,
	}

	b, err := json.Marshal(xrpcc.Auth)
	if err == nil {
		if err := os.WriteFile(authFile, b, 0600); err != nil {
			return nil, err
		}
	}

	return xrpcc, nil
}

func tryCreateXrpcClient(ctx context.Context) (*xrpc.Client, error) {
	xrpcc := &xrpc.Client{
		Host: Host,
	}

	auth, err := cliutil.ReadAuth(authFile)
	if err != nil {
		return nil, err
	}

	xrpcc.Auth = auth

	return xrpcc, nil
}

func parseDate(val string) (string, error) {
	parsedTime, err := time.Parse(time.RFC3339Nano, val)
	if err != nil {
		return "", err
	}
	return parsedTime.Format("15:04:05 02 January"), nil
}
