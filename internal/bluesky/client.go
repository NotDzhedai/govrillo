package bluesky

import (
	"context"

	"github.com/bluesky-social/indigo/xrpc"
)

const Host = "https://bsky.social"

type Client struct {
	xrpclient      *xrpc.Client
	PersonalHandle string
}

func NewClient(handle, password string) (*Client, error) {
	a := &Client{}

	client, err := createXrpcClient(context.Background(), handle, password)
	if err != nil {
		return nil, err
	}

	a.xrpclient = client
	a.PersonalHandle = client.Auth.Handle

	return a, nil
}

func TryCreateClient() (*Client, error) {
	a := &Client{}

	client, err := tryCreateXrpcClient(context.Background())
	if err != nil {
		return nil, err
	}

	a.xrpclient = client
	a.PersonalHandle = client.Auth.Handle

	return a, nil
}
