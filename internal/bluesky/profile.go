package bluesky

import (
	"context"

	"github.com/bluesky-social/indigo/api/bsky"
)

type ProfileData struct {
	Handle      string
	DisplayName string
	Description string
	Follows     int64
	Followers   int64
	Avatar      string
	Banner      string
}

func (a *Client) GetProfileData() (*ProfileData, error) {
	profile, err := bsky.ActorGetProfile(
		context.Background(),
		a.xrpclient,
		a.PersonalHandle,
	)
	if err != nil {
		return nil, err
	}

	res := &ProfileData{
		Handle:      profile.Handle,
		DisplayName: *profile.DisplayName,
		Description: *profile.Description,
		Avatar:      *profile.Avatar,
		Banner:      *profile.Banner,
		Follows:     *profile.FollowsCount,
		Followers:   *profile.FollowersCount,
	}

	return res, nil
}

type ProfileInfo struct {
	Handle      string
	DisplayName string
	Description string
	Avatar      string
	Did         string
}

func stringp(val *string) string {
	if val == nil {
		return ""
	}
	return *val
}

func (a *Client) GetFollows() ([]ProfileInfo, error) {
	res := make([]ProfileInfo, 0)
	var cursor string
	for {
		follows, err := bsky.GraphGetFollows(context.Background(), a.xrpclient, a.PersonalHandle, cursor, 100)
		if err != nil {
			return nil, err
		}

		for _, f := range follows.Follows {
			res = append(res, ProfileInfo{
				Handle:      f.Handle,
				DisplayName: stringp(f.DisplayName),
				Description: stringp(f.Description),
				Avatar:      stringp(f.Avatar),
				Did:         f.Did,
			})
		}

		if follows.Cursor == nil {
			break
		}
		cursor = *follows.Cursor

	}
	return res, nil
}

func (a *Client) GetFollowers() ([]ProfileInfo, error) {
	res := make([]ProfileInfo, 0)
	var cursor string
	for {
		follows, err := bsky.GraphGetFollowers(context.Background(), a.xrpclient, a.PersonalHandle, cursor, 100)
		if err != nil {
			return nil, err
		}

		for _, f := range follows.Followers {
			res = append(res, ProfileInfo{
				Handle:      f.Handle,
				DisplayName: stringp(f.DisplayName),
				Description: stringp(f.Description),
				Avatar:      stringp(f.Avatar),
				Did:         f.Did,
			})
		}

		if follows.Cursor == nil {
			break
		}
		cursor = *follows.Cursor

	}
	return res, nil
}
