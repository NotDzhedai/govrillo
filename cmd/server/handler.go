package main

import (
	"net/http"

	"gitlab.com/NotDzhedai/govrillo/internal/bluesky"
	"gitlab.com/NotDzhedai/govrillo/internal/templates/components"
	"gitlab.com/NotDzhedai/govrillo/internal/templates/pages"
)

type Handler struct {
	App        *bluesky.Client
	IsLoggedIn bool
}

func (h *Handler) loginPage(w http.ResponseWriter, r *http.Request) {
	if err := pages.Login().Render(r.Context(), w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) login(w http.ResponseWriter, r *http.Request) {
	username := r.PostFormValue("username")
	password := r.PostFormValue("password")

	app, err := bluesky.NewClient(username, password)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	h.App = app
	h.IsLoggedIn = true
	h.homePage(w, r)
}

func (h *Handler) homePage(w http.ResponseWriter, r *http.Request) {
	if !h.IsLoggedIn {
		h.loginPage(w, r)
		return
	}

	profile, err := h.App.GetProfileData()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	authorPosts, err := h.App.GetAuthorFeed()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := pages.Home(profile, authorPosts).Render(r.Context(), w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) followsPage(w http.ResponseWriter, r *http.Request) {
	follows, err := h.App.GetFollows()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := components.ModalProfiles(follows, "Follows").Render(r.Context(), w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (h *Handler) followersPage(w http.ResponseWriter, r *http.Request) {
	followers, err := h.App.GetFollowers()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := components.ModalProfiles(followers, "Followers").Render(r.Context(), w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (h *Handler) viewThread(w http.ResponseWriter, r *http.Request) {
	postUri := r.FormValue("post_uri")

	posts, err := h.App.GetPostThread(postUri)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := components.Feed(posts).Render(r.Context(), w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
