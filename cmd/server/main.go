package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/NotDzhedai/govrillo/internal/bluesky"
)

func routes(h *Handler) {
	http.HandleFunc("/", h.homePage)
	http.HandleFunc("/login", h.login)
	http.HandleFunc("/view-thread", h.viewThread)
	http.HandleFunc("/delete-el", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte{})
	})
	http.HandleFunc("/show-follows", h.followsPage)
	http.HandleFunc("/show-followers", h.followersPage)
}

func main() {
	h := &Handler{}

	client, err := bluesky.TryCreateClient()
	if err != nil {
		h.IsLoggedIn = false
	} else {
		h.IsLoggedIn = true
		h.App = client
	}

	routes(h)

	fmt.Println("http://localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))

}
