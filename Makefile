run:
	go run cmd/server/*.go

templ:
	templ generate -path=internal/templates

all: templ run